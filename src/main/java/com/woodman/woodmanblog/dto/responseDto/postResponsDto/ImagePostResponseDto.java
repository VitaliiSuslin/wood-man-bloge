package com.woodman.woodmanblog.dto.responseDto.postResponsDto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImagePostResponseDto {
    @JsonProperty("image_id")
    private Long imageId;
    @JsonProperty("url")
    private String url;

    public ImagePostResponseDto() {
    }

    public ImagePostResponseDto(Long imageId, String url) {
        this.imageId = imageId;
        this.url = url;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
