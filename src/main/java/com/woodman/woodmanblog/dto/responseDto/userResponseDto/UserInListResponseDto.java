package com.woodman.woodmanblog.dto.responseDto.userResponseDto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserInListResponseDto {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("user_name")
    private String userName;

    public UserInListResponseDto() {
    }

    public UserInListResponseDto(Long userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
