package com.woodman.woodmanblog.dto.responseDto.authRsponsetDto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseUserCredential {

    @JsonProperty("token")
    private String token;

    @JsonProperty("token_type")
    private String tokenType = "Bearer";

    public ResponseUserCredential() {
    }

    public ResponseUserCredential(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
