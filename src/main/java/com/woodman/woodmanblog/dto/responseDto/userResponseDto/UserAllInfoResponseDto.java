package com.woodman.woodmanblog.dto.responseDto.userResponseDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.woodman.woodmanblog.dao.entity.user.Role;

import java.util.Set;

public class UserAllInfoResponseDto {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("is_user_non_unlock")
    private boolean isUserNonLocke;

    @JsonProperty("is_user_enable")
    private boolean isUserEnable;

    @JsonProperty("user_roles")
    private Set<Role> userRoles;

    public UserAllInfoResponseDto() {
    }

    public UserAllInfoResponseDto(Long userId, String userName, boolean isUserNonLocke, boolean isUserEnable, Set<Role> userRoles) {
        this.userId = userId;
        this.userName = userName;
        this.isUserNonLocke = isUserNonLocke;
        this.isUserEnable = isUserEnable;
        this.userRoles = userRoles;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isUserNonLocke() {
        return isUserNonLocke;
    }

    public void setUserNonLocke(boolean userNonLocke) {
        isUserNonLocke = userNonLocke;
    }

    public boolean isUserEnable() {
        return isUserEnable;
    }

    public void setUserEnable(boolean userEnable) {
        isUserEnable = userEnable;
    }

    public Set<Role> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<Role> userRoles) {
        this.userRoles = userRoles;
    }
}
