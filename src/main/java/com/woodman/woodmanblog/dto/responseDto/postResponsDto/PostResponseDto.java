package com.woodman.woodmanblog.dto.responseDto.postResponsDto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PostResponseDto {

    @JsonProperty("post_id")
    private Long postId;

    @JsonProperty("title")
    private String title;

    @JsonProperty("content")
    private String content;

    @JsonProperty("images")
    private List<ImagePostResponseDto> imageToPostResponseList;

    public PostResponseDto() {
    }

    public PostResponseDto(Long postId, String title, String content, List<ImagePostResponseDto> imageToPostResponseList) {
        this.postId = postId;
        this.title = title;
        this.content = content;
        this.imageToPostResponseList = imageToPostResponseList;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ImagePostResponseDto> getImageToPostResponseList() {
        return imageToPostResponseList;
    }

    public void setImageToPostResponseList(List<ImagePostResponseDto> imageToPostResponseList) {
        this.imageToPostResponseList = imageToPostResponseList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
