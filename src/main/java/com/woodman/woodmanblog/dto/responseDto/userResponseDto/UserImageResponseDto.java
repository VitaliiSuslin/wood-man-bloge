package com.woodman.woodmanblog.dto.responseDto.userResponseDto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserImageResponseDto {
    @JsonProperty("image_id")
    private Long imageId;
    @JsonProperty("url")
    private String url;

    public UserImageResponseDto() {
    }

    public UserImageResponseDto(Long imageId, String url) {
        this.imageId = imageId;
        this.url = url;
    }
}
