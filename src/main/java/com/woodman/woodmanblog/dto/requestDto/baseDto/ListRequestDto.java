package com.woodman.woodmanblog.dto.requestDto.baseDto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ListRequestDto {
    @JsonProperty("size")
    private int size;

    @JsonProperty("page")
    private int page;

    @JsonProperty("sort_field")
    @NotNull
    @NotBlank
    private String sortField;

    @JsonProperty("sort_by")
    @NotNull
    @NotBlank
    private SortBy sortBy;

    public ListRequestDto(int size, int page, String sortField, SortBy sortBy) {
        this.size = size;
        this.page = page;
        this.sortField = sortField;
        this.sortBy = sortBy;
    }

    public ListRequestDto() {
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public SortBy getSortBy() {
        return sortBy;
    }

    public void setSortBy(SortBy sortBy) {
        this.sortBy = sortBy;
    }
}
