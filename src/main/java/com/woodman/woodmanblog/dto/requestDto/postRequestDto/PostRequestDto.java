package com.woodman.woodmanblog.dto.requestDto.postRequestDto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class PostRequestDto {

    @JsonProperty("user_id")
    @NotNull
    @NotBlank
    private Long userId;

    @JsonProperty("title")
    @NotNull
    @NotBlank
    private String title;

    @JsonProperty("images")
    private List<ImagePostRequest> imagePostRequestList;

    @JsonProperty("content")
    @NotNull
    @NotBlank
    private String content;

    public PostRequestDto() {
    }

    public PostRequestDto(Long userId, String title, List<ImagePostRequest> imagePostRequestList, String content) {
        this.userId = userId;
        this.title = title;
        this.imagePostRequestList = imagePostRequestList;
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ImagePostRequest> getImagePostRequestList() {
        return imagePostRequestList;
    }

    public void setImagePostRequestList(List<ImagePostRequest> imagePostRequestList) {
        this.imagePostRequestList = imagePostRequestList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
