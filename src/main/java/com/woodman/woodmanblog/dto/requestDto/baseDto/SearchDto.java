package com.woodman.woodmanblog.dto.requestDto.baseDto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SearchDto extends ListRequestDto {
    @JsonProperty("search_phrase")
    @NotNull
    @NotBlank
    private String searchPhrase;

    public SearchDto() {
    }

    public SearchDto(int size, int page, String sortField, SortBy sortBy, String searchPhrase) {
        super(size, page, sortField, sortBy);
        this.searchPhrase = searchPhrase;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }
}
