package com.woodman.woodmanblog.dto.requestDto.postRequestDto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ImagePostRequest {
    @JsonProperty("url")
    @NotNull
    @NotBlank
    private String url;

    public ImagePostRequest() {
    }

    public ImagePostRequest(@NotNull @NotBlank String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
