package com.woodman.woodmanblog.dto.requestDto.baseDto;

public enum SortBy {
    ASC,
    DESC
}
