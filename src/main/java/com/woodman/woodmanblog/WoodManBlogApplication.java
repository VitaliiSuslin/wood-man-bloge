package com.woodman.woodmanblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodManBlogApplication {
    public static void main(String[] args) {
        SpringApplication.run(WoodManBlogApplication.class, args);
    }
}
