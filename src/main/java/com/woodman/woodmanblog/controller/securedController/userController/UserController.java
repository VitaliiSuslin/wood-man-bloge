package com.woodman.woodmanblog.controller.securedController.userController;

import com.woodman.woodmanblog.dto.requestDto.baseDto.ListRequestDto;
import com.woodman.woodmanblog.dto.requestDto.baseDto.SearchDto;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserAllInfoResponseDto;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserInListResponseDto;
import com.woodman.woodmanblog.service.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/user/")
public class UserController {

    private final UserService mUserService;

    @Autowired
    public UserController(UserService userService) {
        this.mUserService = userService;
    }

    @PostMapping("get_users")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<List<UserInListResponseDto>> getUsers(@Valid ListRequestDto listRequestDto) {
        return ResponseEntity.ok(mUserService.getUsers(listRequestDto));
    }

    @PostMapping("search_users")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<List<UserInListResponseDto>> searchUsers(@Valid SearchDto searchDto) {
        return ResponseEntity.ok(mUserService.getUsersByParams(searchDto));
    }

    @GetMapping("get_user_details/id={id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<UserAllInfoResponseDto> getUserDetails(@PathVariable("id") long id) {
        return ResponseEntity.ok(mUserService.getUserInfo(id));
    }

}
