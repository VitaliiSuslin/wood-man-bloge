package com.woodman.woodmanblog.controller.securedController.postsController;

import com.woodman.woodmanblog.dto.requestDto.postRequestDto.PostRequestDto;
import com.woodman.woodmanblog.dto.responseDto.postResponsDto.PostResponseDto;
import com.woodman.woodmanblog.service.postService.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/posts/")
public class PostsController {

    private final PostService mPostService;

    @Autowired
    public PostsController(PostService postService) {
        this.mPostService = postService;
    }

    @PostMapping("/creating_post")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<PostResponseDto> creatingPost(@Valid PostRequestDto postRequestDto) {
        return ResponseEntity.ok(mPostService.createPost(postRequestDto));
    }

    @GetMapping("/id={id}")
    public ResponseEntity<PostResponseDto> getPostById(@PathVariable("id") long id) {
        return ResponseEntity.ok(mPostService.getPost(id));
    }

}
