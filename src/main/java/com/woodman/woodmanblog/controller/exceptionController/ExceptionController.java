package com.woodman.woodmanblog.controller.exceptionController;

import com.woodman.woodmanblog.dto.responseDto.errorResponseDto.ErrorResponse;
import com.woodman.woodmanblog.exception.BadRequestException;
import com.woodman.woodmanblog.util.ErrorResponseCode;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value
            = {IllegalArgumentException.class, IllegalStateException.class})
    public ResponseEntity<?> handleConflict(RuntimeException ex, WebRequest request, ErrorResponseCode errorCode, String message) {
        return handleExceptionInternal(ex, new ErrorResponse(errorCode, message), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = ChangeSetPersister.NotFoundException.class)
    public ResponseEntity<?> handleNotFound(ChangeSetPersister.NotFoundException exception, WebRequest request) {
        return handleExceptionInternal(
                exception,
                new ErrorResponse(ErrorResponseCode.NOT_FOUND, exception.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = BadRequestException.class)
    public ResponseEntity<?> handleBadRequest(BadRequestException exception, WebRequest request) {
        return handleExceptionInternal(
                exception,
                new ErrorResponse(ErrorResponseCode.BAD_REQUEST, exception.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST, request);
    }

//    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
//    public ResponseEntity<?> handleMethodNotAllowed(HttpRequestMethodNotSupportedException exception, WebRequest request) {
//        return handleExceptionInternal(
//                exception,
//                new ErrorResponse(ErrorResponseCode.METHOD_NOT_ALLOWED, exception.getMessage()),
//                new HttpHeaders(),
//                HttpStatus.METHOD_NOT_ALLOWED, request);
//    }
//
//    @ExceptionHandler(value = HttpMediaTypeNotAcceptableException.class)
//    protected ResponseEntity<?> handleMedicaTypeNotAcceptable(HttpMediaTypeNotAcceptableException exception, WebRequest request) {
//        return handleExceptionInternal(
//                exception,
//                new ErrorResponse(ErrorResponseCode.UNSUPPORTED_MEDIA_TYPE.getErrorCode(), exception.getMessage()),
//                new HttpHeaders(),
//                HttpStatus.UNSUPPORTED_MEDIA_TYPE, request);
//    }
//
//    @ExceptionHandler(value = .class)
//    protected ResponseEntity<?> handleForbiddenTargetException(ForbiddenTargetException exception, WebRequest request) {
//        return handleExceptionInternal(
//                exception,
//                new ErrorResponse(ErrorResponseCode.FORBIDDEN.getErrorCode(), exception.getMessage()),
//                new HttpHeaders(),
//                HttpStatus.FORBIDDEN, request);
//    }
//
//    @ExceptionHandler(value = ExecutionControl.InternalException.class)
//    protected ResponseEntity<?> handleInternalException(ExecutionControl.InternalException exception, WebRequest request) {
//        return handleExceptionInternal(
//                exception,
//                new ErrorResponse(ErrorResponseCode.INTERNAL_SERVER_ERROR.getErrorCode(), exception.getMessage()),
//                new HttpHeaders(),
//                HttpStatus.INTERNAL_SERVER_ERROR, request);
//    }
}
