package com.woodman.woodmanblog.converters;

import com.woodman.woodmanblog.dao.entity.image.Image;
import com.woodman.woodmanblog.dao.entity.post.Post;
import com.woodman.woodmanblog.dto.requestDto.postRequestDto.PostRequestDto;
import com.woodman.woodmanblog.dto.responseDto.postResponsDto.PostResponseDto;
import org.springframework.data.util.Pair;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;


public final class PostConverter {

    @NotNull
    public static Pair<Post, List<Image>> convertPostRequestDto(@NotNull PostRequestDto postRequestDto) {
        return Pair.of(
                new Post(postRequestDto.getTitle(),
                        postRequestDto.getContent()),
                postRequestDto.getImagePostRequestList().stream().map(ImageConverter::convertImagePostResponseDto).collect(Collectors.toList())
        );
    }

    @NotNull
    public static PostResponseDto convertPostToPostResponseDto(@NotNull Post post) {
        return new PostResponseDto(
                post.getId(),
                post.getTitle(),
                post.getContent(),
                post.getImages().stream().map(ImageConverter::convertImageToImagePostResponseDto).collect(Collectors.toList())
        );
    }
}
