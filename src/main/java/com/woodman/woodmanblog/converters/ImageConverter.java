package com.woodman.woodmanblog.converters;

import com.woodman.woodmanblog.dao.entity.image.Image;
import com.woodman.woodmanblog.dto.requestDto.postRequestDto.ImagePostRequest;
import com.woodman.woodmanblog.dto.responseDto.postResponsDto.ImagePostResponseDto;

import javax.validation.constraints.NotNull;

public final class ImageConverter {

    @NotNull
    public static Image convertImagePostResponseDto(@NotNull ImagePostRequest imagePostRequest) {
        return new Image(imagePostRequest.getUrl());
    }

    @NotNull
    public static ImagePostResponseDto convertImageToImagePostResponseDto(@NotNull Image image) {
        return new ImagePostResponseDto(image.getId(), image.getUrl());
    }
}
