package com.woodman.woodmanblog.converters;

import com.woodman.woodmanblog.dao.entity.user.User;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserAllInfoResponseDto;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserInListResponseDto;

import javax.validation.constraints.NotNull;

public final class UserConverter {

    public static UserInListResponseDto convertUserEntityToUserInListDto(@NotNull User user) {
        return new UserInListResponseDto(
                user.getId(),
                user.getUserName()
        );
    }

    public static UserAllInfoResponseDto convertUserEntityToUserAllInfoDto(@NotNull User user) {
        return new UserAllInfoResponseDto(
                user.getId(),
                user.getUserName(),
                user.isUserNonLocke(),
                user.isUserEnable(),
                user.getRoles()
        );
    }
}
