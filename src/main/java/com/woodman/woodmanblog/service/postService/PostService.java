package com.woodman.woodmanblog.service.postService;

import com.woodman.woodmanblog.dto.requestDto.baseDto.ListRequestDto;
import com.woodman.woodmanblog.dto.requestDto.baseDto.SearchDto;
import com.woodman.woodmanblog.dto.requestDto.postRequestDto.PostRequestDto;
import com.woodman.woodmanblog.dto.responseDto.postResponsDto.PostResponseDto;

import java.util.List;

public interface PostService {

    PostResponseDto createPost(PostRequestDto postRequestDto);

    PostResponseDto getPost(Long id);

    List<PostResponseDto> getPosts(ListRequestDto listRequestDto);

    List<PostResponseDto> searchPosts(SearchDto searchDto);

    PostResponseDto updatePost(Long postId, PostRequestDto postRequestDto);

    boolean deletePost(Long id);

}
