package com.woodman.woodmanblog.service.postService.impl;

import com.woodman.woodmanblog.converters.PostConverter;
import com.woodman.woodmanblog.dao.repository.imageRepository.ImageRepository;
import com.woodman.woodmanblog.dao.repository.postRepository.PostRepository;
import com.woodman.woodmanblog.dao.repository.userRepository.UserRepository;
import com.woodman.woodmanblog.dto.requestDto.baseDto.ListRequestDto;
import com.woodman.woodmanblog.dto.requestDto.baseDto.SearchDto;
import com.woodman.woodmanblog.dto.requestDto.postRequestDto.PostRequestDto;
import com.woodman.woodmanblog.dto.responseDto.postResponsDto.PostResponseDto;
import com.woodman.woodmanblog.service.postService.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private final PostRepository mPostRepository;

    private final ImageRepository mImageRepository;

    private final UserRepository mUserRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository,
                           ImageRepository imageRepository,
                           UserRepository userRepository) {
        this.mPostRepository = postRepository;
        this.mImageRepository = imageRepository;
        this.mUserRepository = userRepository;
    }


    @Override
    public PostResponseDto createPost(PostRequestDto postRequestDto) {

        var user = mUserRepository.findById(postRequestDto.getUserId()).orElseThrow(
                () -> new EntityNotFoundException("User by id:[ " + postRequestDto.getUserId() + " ] not found!"));

        var dataPair = PostConverter.convertPostRequestDto(postRequestDto);

        var createdPost = dataPair.getFirst();

        createdPost.setUser(user);

        var newImages = mImageRepository.saveAll(dataPair.getSecond());

        createdPost.setImages(newImages);

        var newPost = mPostRepository.save(createdPost);

        return PostConverter.convertPostToPostResponseDto(newPost);
    }

    @Override
    public PostResponseDto getPost(Long id) {
        var post = mPostRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Post by id:[ " + id + "] not found!"));
        return PostConverter.convertPostToPostResponseDto(post);
    }

    @Override
    public List<PostResponseDto> getPosts(ListRequestDto listRequestDto) {
        var posts = mPostRepository.findAll(PageRequest.of(listRequestDto.getPage(),
                listRequestDto.getSize(),
                Sort.by(listRequestDto.getSortBy().name(),
                        listRequestDto.getSortField())));

        return posts.stream().map(PostConverter::convertPostToPostResponseDto).collect(Collectors.toList());
    }

    @Override
    public List<PostResponseDto> searchPosts(SearchDto searchDto) {
        var posts = mPostRepository.findAll(PageRequest.of(searchDto.getPage(),
                searchDto.getSize(),
                Sort.by(searchDto.getSortBy().name(),
                        searchDto.getSortField())));

        return posts.stream().map(PostConverter::convertPostToPostResponseDto).collect(Collectors.toList());
    }

    @Override
    public PostResponseDto updatePost(Long id, PostRequestDto postRequestDto) {
        var post = mPostRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Post by id:[ " + id + "] not found!"));
        return null;
    }

    @Override
    public boolean deletePost(Long id) {
        var post = mPostRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Post by id:[ " + id + "] not found!"));

        mPostRepository.delete(post);

        return true;
    }
}
