package com.woodman.woodmanblog.service.userService;

import com.woodman.woodmanblog.dto.requestDto.authRequestDto.LoggingUser;
import com.woodman.woodmanblog.dto.requestDto.authRequestDto.RegisterUserRequest;
import com.woodman.woodmanblog.dto.requestDto.baseDto.ListRequestDto;
import com.woodman.woodmanblog.dto.requestDto.baseDto.SearchDto;
import com.woodman.woodmanblog.dto.responseDto.ApiResponse;
import com.woodman.woodmanblog.dto.responseDto.authRsponsetDto.ResponseUserCredential;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserAllInfoResponseDto;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserInListResponseDto;
import com.woodman.woodmanblog.exception.BadRequestException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserService {

    ApiResponse createNewUser(RegisterUserRequest registerUserRequest) throws BadRequestException;

    ResponseUserCredential signIn(LoggingUser name) throws UsernameNotFoundException;

    UserDetails loggingById(Long id) throws UsernameNotFoundException;

    UserAllInfoResponseDto getUserInfo(Long userId);

    List<UserInListResponseDto> getUsers(ListRequestDto listRequestDto);

    List<UserInListResponseDto> getUsersByParams(SearchDto searchDto);
}
