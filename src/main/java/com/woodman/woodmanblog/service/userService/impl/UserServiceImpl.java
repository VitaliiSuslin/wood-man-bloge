package com.woodman.woodmanblog.service.userService.impl;

import com.woodman.woodmanblog.converters.UserConverter;
import com.woodman.woodmanblog.dao.entity.user.User;
import com.woodman.woodmanblog.dao.enumObjects.RoleName;
import com.woodman.woodmanblog.dao.repository.authRepository.RoleRepository;
import com.woodman.woodmanblog.dao.repository.userRepository.UserRepository;
import com.woodman.woodmanblog.dto.requestDto.authRequestDto.LoggingUser;
import com.woodman.woodmanblog.dto.requestDto.authRequestDto.RegisterUserRequest;
import com.woodman.woodmanblog.dto.requestDto.baseDto.ListRequestDto;
import com.woodman.woodmanblog.dto.requestDto.baseDto.SearchDto;
import com.woodman.woodmanblog.dto.responseDto.ApiResponse;
import com.woodman.woodmanblog.dto.responseDto.authRsponsetDto.ResponseUserCredential;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserAllInfoResponseDto;
import com.woodman.woodmanblog.dto.responseDto.userResponseDto.UserInListResponseDto;
import com.woodman.woodmanblog.exception.BadRequestException;
import com.woodman.woodmanblog.security.JwtTokenProvider;
import com.woodman.woodmanblog.security.UserPrincipal;
import com.woodman.woodmanblog.service.userService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository mUserRepository;
    private final RoleRepository mRoleRepository;
    private final AuthenticationManager mAuthenticationManager;
    private final JwtTokenProvider mTokenProvider;
    private final PasswordEncoder mPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, AuthenticationManager authenticationManager, JwtTokenProvider tokenProvider, PasswordEncoder passwordEncoder) {
        mUserRepository = userRepository;
        mRoleRepository = roleRepository;
        mAuthenticationManager = authenticationManager;
        mTokenProvider = tokenProvider;
        mPasswordEncoder = passwordEncoder;
    }

    @Override
    public UserAllInfoResponseDto getUserInfo(Long userId) {

        var user = mUserRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("User by id:[ " + userId + " ] not found!"));

        return UserConverter.convertUserEntityToUserAllInfoDto(user);
    }

    @Override
    public List<UserInListResponseDto> getUsers(ListRequestDto listRequestDto) {

        var userList = mUserRepository.findAll(
                PageRequest.of(listRequestDto.getPage(),
                        listRequestDto.getSize(),
                        Sort.by(listRequestDto.getSortBy().name(),
                                listRequestDto.getSortField())));

        return userList.stream().map(UserConverter::convertUserEntityToUserInListDto).collect(Collectors.toList());
    }

    @Override
    public List<UserInListResponseDto> getUsersByParams(SearchDto searchDto) {

        var userList = mUserRepository.findAll(
                Example.of(new User()),
                PageRequest.of(searchDto.getPage(),
                        searchDto.getSize(),
                        Sort.by(searchDto.getSortBy().name(),
                                searchDto.getSortField())));

        return userList.stream().map(UserConverter::convertUserEntityToUserInListDto).collect(Collectors.toList());
    }

    /**
     * Default user role is "ROLE_USER"
     */
    @Override
    public ApiResponse createNewUser(RegisterUserRequest registerUserRequest) throws BadRequestException {
        var isUserExists = mUserRepository.findByUserName(registerUserRequest.getUserName());

        if (isUserExists.isPresent()) throw new BadRequestException("This username is already exists!!!");

        if (!registerUserRequest.getUserPassword().equals(registerUserRequest.getConfirmPassword()))
            throw new BadRequestException("Confirm password not same with password!");

        var userRole = mRoleRepository.findByRoleName(RoleName.ROLE_USER).orElseThrow(() -> new BadRequestException("User roles not found"));

        var encryptingString = mPasswordEncoder.encode(registerUserRequest.getUserPassword());

        var user = mUserRepository.save(new User(registerUserRequest.getUserName(), encryptingString, Collections.singleton(userRole)));

        return new ApiResponse("User " + user.getUserName() + " successfully registered!!!");
    }

    @Override
    public ResponseUserCredential signIn(LoggingUser loggingUser) throws UsernameNotFoundException {

        mUserRepository.findByUserName(loggingUser.getUserName())
                .orElseThrow(() -> new UsernameNotFoundException("User by id:[ " + loggingUser.getUserName() + " ] not found!"));

        var authentication = mAuthenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        loggingUser.getUserName(),
                        loggingUser.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return new ResponseUserCredential(mTokenProvider.generateToken(authentication));
    }


    @Override
    @Transactional
    public UserDetails loggingById(Long id) throws UsernameNotFoundException {
        var user = mUserRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User by id:[ " + id + " ] not found!"));
        return UserPrincipal.create(user);
    }

}
