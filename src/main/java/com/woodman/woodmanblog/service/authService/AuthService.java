package com.woodman.woodmanblog.service.authService;

import com.woodman.woodmanblog.dto.requestDto.authRequestDto.LoggingUser;
import com.woodman.woodmanblog.dto.requestDto.authRequestDto.RegisterUserRequest;
import com.woodman.woodmanblog.dto.responseDto.ApiResponse;
import com.woodman.woodmanblog.dto.responseDto.authRsponsetDto.ResponseUserCredential;
import com.woodman.woodmanblog.exception.BadRequestException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface AuthService extends UserDetailsService{
}
