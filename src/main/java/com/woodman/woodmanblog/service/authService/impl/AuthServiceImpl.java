package com.woodman.woodmanblog.service.authService.impl;

import com.woodman.woodmanblog.dao.repository.userRepository.UserRepository;
import com.woodman.woodmanblog.security.UserPrincipal;
import com.woodman.woodmanblog.service.authService.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository mUserRepository;

    @Autowired
    public AuthServiceImpl(UserRepository mUserRepository) {
        this.mUserRepository = mUserRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = mUserRepository.findByUserName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User by id:[ " + username + " ] not found!"));
        return UserPrincipal.create(user);
    }
}
