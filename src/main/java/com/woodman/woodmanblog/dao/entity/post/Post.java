package com.woodman.woodmanblog.dao.entity.post;

import com.woodman.woodmanblog.dao.entity.image.Image;
import com.woodman.woodmanblog.dao.entity.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @ManyToOne
    @JoinTable(name = "user")
    private User user;

    @OneToMany
    private List<Image> images;

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Post(String title, String content, User user, List<Image> images) {
        this.title = title;
        this.content = content;
        this.user = user;
        this.images = images;
    }

    public Post() {
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
