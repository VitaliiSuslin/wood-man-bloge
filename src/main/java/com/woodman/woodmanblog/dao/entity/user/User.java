package com.woodman.woodmanblog.dao.entity.user;

import com.woodman.woodmanblog.dao.entity.DateAudit;
import com.woodman.woodmanblog.dao.entity.image.Image;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_name", unique = true)
    private String userName;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "is_account_non_locke", nullable = false, columnDefinition = "boolean default 1")
    private boolean isUserNonLocke;

    @Column(name = "is_user_enable", nullable = false, columnDefinition = "boolean default 1")
    private boolean isUserEnable;

    @Column(name = "is_account_non_expired", nullable = false, columnDefinition = "boolean default 1")
    private boolean isAccountNonExpired;

    @Column(name = "is_credentials_non_expired", nullable = false, columnDefinition = "boolean default 1")
    private boolean isCredentialsNonExpired;

    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @OneToMany
    @JoinTable(name = "image")
    private List<Image> images;

    public User() {
    }

    public User(String userName, String userPassword, Set<Role> roles) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.roles = roles;
    }

    public User(String userName, String userPassword, Set<Role> roles, List<Image> images) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.roles = roles;
        this.images = images;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public boolean isUserNonLocke() {
        return isUserNonLocke;
    }

    public void setUserNonLocke(boolean userNonLocke) {
        isUserNonLocke = userNonLocke;
    }

    public boolean isUserEnable() {
        return isUserEnable;
    }

    public void setUserEnable(boolean userEnable) {
        isUserEnable = userEnable;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }
}
