package com.woodman.woodmanblog.dao.repository.postRepository;

import com.woodman.woodmanblog.dao.entity.post.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
}
