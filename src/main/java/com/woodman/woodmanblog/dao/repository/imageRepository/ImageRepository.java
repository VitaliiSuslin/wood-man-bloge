package com.woodman.woodmanblog.dao.repository.imageRepository;

import com.woodman.woodmanblog.dao.entity.image.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
}
