package com.woodman.woodmanblog.util.customAnotations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@NotNull
@NotBlank
public @interface NotNullOrNotBlank {
}
