package com.woodman.woodmanblog.security;

import com.woodman.woodmanblog.service.userService.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//TODO: Modify to correct and nice code
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String LOG_TAG = "JwtAuthenticationFilter -> ";
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    @Autowired
    private JwtTokenProvider mTokenProvider;

    @Autowired
    private UserService mUserService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        authFilter(request, response);

        filterChain.doFilter(request, response);
    }

    private String getJwt(String authHeader) {
        if (!authHeader.startsWith("Bearer "))
            return null;
        return authHeader.substring(7);
    }

    //TODO: checking is user token not expired, or account not expired
    private void authFilter(HttpServletRequest request, HttpServletResponse response) {
        var authHeader = request.getHeader(HEADER_AUTHORIZATION);

        if (authHeader == null) return;

        if (authHeader.isEmpty()) {
            response.setStatus(400);
            logger.error("auth header is empty");
            return;
        }

        var jwt = getJwt(authHeader);

        if (jwt == null) {
            response.setStatus(400);
            logger.error("jwt == null");
            return;
        }

        try {
            var userClaims = mTokenProvider.getUserClaimsFromJWT(jwt);
            var userDetails = mUserService.loggingById(Long.parseLong(userClaims.getSubject()));
            var validationJwt = mTokenProvider.validateToken(jwt);

            if (userDetails.isAccountNonExpired() && validationJwt.isValid()) {

                if (SecurityContextHolder.getContext().getAuthentication() == null) {

                    var authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            } else {
                response.setStatus(401);
                logger.warn("Token is not valid");
            }
        } catch (Exception e) {
            logger.error(LOG_TAG, e);
        }
    }
}
